import asyncio
import logging
import typing

import aiohttp
import discord
from discord.ext import commands
from discord.ext.commands import Context

from .urlformat import get_formats

__version__ = "2.0.0"

_DEFAULT_CONFIG = {
    "USER_AGENT": f"ArgHelper/{__version__} (aiohttp/{aiohttp.__version__})"
}

_log = logging.getLogger(__name__)
_log.setLevel(logging.DEBUG)


class ArgHelper(commands.Cog):
    def __init__(self, cache:dict={}, config:dict={}):
        self.cache = cache
        self.config = dict(_DEFAULT_CONFIG)
        self.config.update(config)
        self.session = None

        _log.info(f"user_agent = {self.config['USER_AGENT']!r}")

    @commands.Cog.listener("on_ready")    
    async def init_session(self):
        if self.session is None:
            self.session = aiohttp.ClientSession(
                cookie_jar = aiohttp.DummyCookieJar(),
                headers = {
                    "User-Agent": self.config["USER_AGENT"]
                },
                conn_timeout=1,
            )
    
    @commands.command()
    async def check(self, ctx:commands.Context, *slugs):
        if self.session is None:
            ctx.reply("Not ready yet, gimme a few seconds.")
            return
        
        for slug in slugs:
            asyncio.create_task(self.check_one(ctx, slug))
        
    async def check_one(self, ctx:Context, slug:str):
        with ctx.typing():
            _log.debug(f"checking slug '{slug}'")
            formats = get_formats()
            lines = []
            rejected = 0
            accepted = 0
            nformats = 0
            timeouts = 0

            for fmt in formats:
                nformats += 1
                results = await fmt(slug, self.session)
                #_log.debug(f"{fmt!r} -> {results}")
                if results is None:
                    rejected += 1
                else:
                    accepted += 1
                    async for r in results:
                        if isinstance(r, Exception):
                            timeouts += 1
                        else:
                            lines.append(f"<{r}>")

            _log.debug(f"slug '{slug}' got {nformats=}, {rejected=}, {accepted=} {len(lines)=}")

            if rejected == nformats:
                # all formats rejected slug
                await ctx.reply(f"`{slug}`: no formats with that length")
                return
            

            extras = []

            if rejected > 0:
                extras.append(f"{rejected} url format{'s' if rejected!=1 else ''} skipped")
            
            if timeouts > 0:
                extras.append(f"{timeouts} connection{'s' if timeouts!=1 else ''} timed out")
            
            if extras:
                appendix = f"\n*({','.join(extras)})*"
            else:
                appendix = ""

            if len(lines) == 0:
                # no results but because the slug doesn't exist
                await ctx.reply(
                    f"`{slug}`: nothing found"
                + appendix
                )
            else:
                await ctx.reply(
                    "\n".join(lines)
                + appendix    
                )
