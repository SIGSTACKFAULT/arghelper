import abc
import aiohttp
import asyncio
import logging
from typing import List, Dict, Sequence, Optional

from aiohttp import ClientSession, ClientResponse
from braceexpand import braceexpand

_LOGGER = logging.getLogger(__name__)
_LOGGER.setLevel(logging.DEBUG)

IMGUR_LENGTHS = range(5,9)

def get_formats():
    yield SaneURLFormat("https://i.imgur.com/%s.{png,jpg,gif}", lengths=IMGUR_LENGTHS)
    yield SaneURLFormat("https://imgur.com/a/%s", lengths=IMGUR_LENGTHS)
    #yield SaneURLFormat("https://imgur.com/%s", lengths=[5]) # always 200
    yield SaneURLFormat("https://tinyimg.io/i/%s.{png,jpg,gif}", lengths=[7])
    yield SaneURLFormat("https://i.cubeupload.com/%s.{png,jpg,gif}", lengths=[6])
    yield YouTubeURLFormat("https://youtube.com/watch?v=%s", "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=%s&format=json", lengths=[11])
    #yield f"https://youtube.com/watch?v={slug}"
    #yield f"https://imgur.com/{slug}"
    #for ext in "png","jpg","gif":
        #yield f"https://i.imgur.com/{slug}.{ext}"


class URLFormat(abc.ABC):
    def __init__(self, lenghts:Sequence[int]=None):
        self.lengths = lenghts
    
    async def __call__(self, slug:str, session:ClientSession) -> Dict[str, int]:
        """deals with length range functionality then hands off to handle_slug"""
        if self.lengths and len(slug) not in self.lengths:
            _LOGGER.debug(f"{self!r}: ignoring slug '{slug}'; out of range.")
            return None
        else:
            _LOGGER.debug(f"{self!r}: processing slug '{slug}'")
            return self.handle_slug(slug, session)

    @abc.abstractmethod
    async def handle_slug(self, slug:str, session:ClientSession): pass

    @abc.abstractmethod
    async def _acceptp(self, result:ClientResponse) -> bool: pass
    
    def __str__(self):
        return self.__repr__()


class SaneURLFormat(URLFormat):
    def __init__(
        self,
        url:str,
        codes:List[int]=[200],
        lengths:Optional[Sequence[int]]=None
    ):
        super().__init__(lengths)
        self.url = url
        self.urls = list(braceexpand(url))
        self.codes = codes
        self.lengths = lengths

    def __repr__(self):
        return f"SaneURLFormat({self.url!r}, codes={self.codes!r}, lengths={self.lengths!r})"
    
    def _format(self, slug):
        """return URLs with slug inserted"""
        for url in self.urls:
            yield url % slug
    
    async def _acceptp(self, result:ClientResponse):
        return result.status in self.codes
        
    async def handle_slug(self, slug:str, session:ClientSession) -> Dict[str,int]:
        urls = self._format(slug)
        urls = list(urls)
        if urls:
            #_LOGGER.debug(f"SaneURLFormat: slug '{slug}', urls {list(urls)}")
            # generate a bunch of awaitables but don't run them yet
            coros = [session.head(url) for url in urls]

            # now actually run the coros and process them in whatever order
            for coro in coros:
                try:
                    result:ClientResponse = await coro
                    _LOGGER.debug(f"{result.url!r} -> {result.status}")
                    if await self._acceptp(result):
                        yield result.url
                except aiohttp.ClientConnectionError as ex:
                    yield ex


class WeirdURLFormat(URLFormat):
    """ special format for YouTube - checks one url but returns a different one"""
    def __init__(self, fake, real, lengths=[11]):
        super().__init__(lengths)
        self.fake = fake
        self.real = real
        self.lengths = lengths
    
    def __repr__(self):
        return f"WeirdURLFormat({self.fake!r}, {self.real!r}, lengths = {self.lengths!r})"

    async def handle_slug(self, slug:str, session:ClientSession):
        url = self.real % slug
        async with session.get(url) as r:
            if await self._acceptp(r):
                yield self.fake % slug


class YouTubeURLFormat(WeirdURLFormat):
    async def _acceptp(self, result:ClientResponse):
        return result.status == 200
