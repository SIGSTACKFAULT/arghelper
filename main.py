#!/usr/bin/env python3
import asyncio
import logging

import click
from discord.ext import commands

import arghelper

logging.basicConfig(level=logging.DEBUG)
#logging.getLogger().setLevel(logging.DEBUG)
logging.getLogger("discord").setLevel(logging.WARN)
logging.getLogger("asyncio").setLevel(logging.WARN)

async def amain(command_prefix="%"):

    bot = commands.Bot(
        command_prefix=command_prefix
    )

    bot.add_cog(
        arghelper.ArgHelper(
            config = {
                "SLUGS_SINGLE_FILE": False,
                "URLS_SINGLE_FILE": False
            }
        )
    )

    @bot.listen()
    async def on_ready():
        print("Ready!")

    with open("token") as f:
        token = f.read().strip()

    await bot.start(token)


@click.command()
@click.option("-p", "--prefix", default="%", help="prefix")
def main(prefix):
	print(f"prefix: '{prefix}'")
	asyncio.run(amain(command_prefix=prefix))

main() #pylint: disable=no-value-for-parameter
