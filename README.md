# arghelper
Discord bot to help with generic ARG solving.

![Usage example screenshot](https://i.imgur.com/3lobzrF.png)

## Synopsis

<pre>
%check <em>id...</em>
</pre>

The prefix (default `%`) is configurable with `-p` or `--prefix`.

## Description

Sends url requests to multiple file hosting services.
Current available services:
* imgur.com
* tinyimg.io
* cubeupload.com
* youtube.com

Also tries suffixes, where applicable:

* .png
* .jpg
* .gif

Naturally, it won't try anything like `https://youtube.com/watch?v=dQw4w9WgXcQ.png`

## Fast
It's *blisteringly* fast. It uses aiohttp, which allows all the requests to run at once

## Configurable
Adding new URL formats is easy; they're at the top of `arghelper/urlformat.py`.
It's also possible to write your own functionality to determine if a response is considered "found" or not, if you need that for some reason.
